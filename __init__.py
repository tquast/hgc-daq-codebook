from pprint import pprint
from collections import Counter
from itertools import product
import re    
import networkx as nx
import numpy as np
from tqdm import tqdm
from tabulate import tabulate

#kCode = idle code, i.e. with highest frequency
#we need at least 26 codes - ideally 32
code_priorities = {
	0: "kCode",		#Phase 1
	1: "L1A",		#Phase 1
	2: "PreL1A",	#Phase 1
	3: "L1A+PreL1A",	#Phase 1
	4: "OrbitSync",		#Phase 1
	5: "OrbitSync+L1A",	#Phase 2
	6: "OrbitSync+PreL1A",	#Phase 2
	8: "OrbitSync+L1A+PreL1A",	#Phase 2
	7: "NZS+L1A",	#Phase 2
	11: "NZS+L1A+PreL1A",	
	9: "CalPulseInt",	#Phase 2
	12: "CalPulseInt+L1A",
	13: "CalPulseInt+PreL1A",
	10: "CalPulseExt",	#Phase 2
	14: "CalPulseExt+L1A",
	15: "CalPulseExt+PreL1A",
	16: "OrbitCounterReset+OrbitSync",
	17:	"EventCounterReset",
	18:	"EventBufferClear",
	19:	"LinkResetROCT",
	20:	"LinkResetROCD",
	21:	"LinkResetECONT",
	22:	"LinkResetECOND",
	23:	"ChipSync",
	24:	"CalPulseInt+L1A+PreL1A",
	25:	"CalPulseExt+L1A+PreL1A",
	26:	"Spare-1",
	27:	"Spare-2",
	28:	"Spare-3",
	29:	"Spare-4",
	30:	"Spare-5",
	31:	"Spare-6"
}


def disparity(code):
    return code.count('1')-code.count('0')

def countKCodeInString(kCode, string):
	return len(re.findall('(?=%s)'%kCode, string))

def isKCodeInString(kCode, string):
    return countKCodeInString(kCode, string) != 0

def invertCode(code):
	return code.replace("0", "2").replace("1", "0").replace("2", "1")

def countKCodeInKCodePairs(kCode):
    # In the normal idle stream expect to find the kCode the two times it is there 
    direct = countKCodeInString(kCode, 2*kCode)
    # In the inverted idle stream, we require that it is not present at all
    inverted = countKCodeInString(kCode, 2*invertCode(kCode))
    return (direct, inverted)

def isKCodeGood(kCode):
    (direct, inverted) = countKCodeInKCodePairs(kCode)
    BooleanToStatus = {True: "OK", False: "X"}
    return BooleanToStatus[direct == 2 and inverted == 0]


def hamming_distance(code1, code2):
	return sum(c1 != c2 for c1, c2 in zip(code1, code2))


#1. construct all possible 8-bit codes
allCodes = ['{:08b}'.format(value) for value in range(2**8)]

#2. disparity must be equal to zero (average voltage = 0.5V)
#count the disparity
pprint('Number of 8b codes by disparity:')
pprint(Counter([ disparity(code) for code in allCodes ]))

# Using only balanced codes makes sure that we can keep the line DC-balanced without special care
balancedCodes = [ code for code in allCodes if disparity(code)==0]
pprint('There are %i balanced codes.'%len(balancedCodes))

#3a. clock desynchronisation could occur --> kCode should only appear twice in pair
#3b. inversion: kCode should not appear at all
kCodeStatus = { kCode: isKCodeGood(kCode) for kCode in balancedCodes }
statusKCode = {} 
for kCode in balancedCodes:
    status = isKCodeGood(kCode)
    statusKCode[status] = statusKCode.get(status, [])
    statusKCode[status].append(kCode)
pprint('Status of kCodes regarding their appearing elsewhere in the idle (kCode) stream or in the inverted idle stream:')
pprint(statusKCode)
kCodeCandidates = statusKCode["OK"]


#code candidates for a specific kCode, i.e. those which do not have a collision
codeCandidates = {}		
print "Finding code candidates"
for kCode in tqdm(kCodeCandidates, unit="kCodes"):	
	codeCandidates[kCode] = []		#candidates that do not have bad interference with kCode
	#for candidateCode in kCodeCandidates:		#choose candidates from pool of kCodes ...
	for candidateCode in balancedCodes:		# ... or from the balanced code
		if candidateCode==kCode:
			continue
		good_vertex_to_kCode = True
		good_vertex_to_kCode &= countKCodeInString(kCode, kCode+candidateCode)==1
		good_vertex_to_kCode &= countKCodeInString(kCode, candidateCode+kCode)==1
		good_vertex_to_kCode &= countKCodeInString(candidateCode, kCode+candidateCode)==1
		good_vertex_to_kCode &= countKCodeInString(candidateCode, candidateCode+kCode)==1
		good_vertex_to_kCode &= countKCodeInString(kCode, invertCode(kCode+candidateCode))==0
		good_vertex_to_kCode &= countKCodeInString(kCode, invertCode(candidateCode+kCode))==0
		good_vertex_to_kCode &= countKCodeInString(candidateCode, invertCode(kCode+candidateCode))==0
		good_vertex_to_kCode &= countKCodeInString(candidateCode, invertCode(candidateCode+kCode))==0

		if good_vertex_to_kCode:
			codeCandidates[kCode].append(candidateCode)



#store all hypotheses from all phases
phase1_hypotheses = []
#phase 1: find all codes with hamming distance of at least 6 to the kCode candidate
print "Phase 1"
for kCode in tqdm(kCodeCandidates, unit="kCodes"):	
	phase1_candidates = []
	for candidateCode in codeCandidates[kCode]:
		if hamming_distance(kCode, candidateCode)>=6:
			phase1_candidates.append(candidateCode)

	#require that all Phase-1 code candidates have at hamming distance of at least 4 w.r.t. each other
	tmp_graph = nx.Graph()
	for node1, node2 in product(phase1_candidates, phase1_candidates):
		if int(node1, 2) < int(node2, 2):
			continue
		if hamming_distance(node1, node2)>=4:
			tmp_graph.add_edge(node1, node2)


	cliques = nx.clique.enumerate_all_cliques(tmp_graph)
	if len([clique for clique in cliques])==0:
		continue
	cliques = nx.clique.enumerate_all_cliques(tmp_graph)
	MaxCliqueLength = max([len(clique) for clique in cliques])
	if MaxCliqueLength<4:		#we want at least 4 codes with distance==6 to kCode and ==4 among each other
		continue
	cliques = nx.clique.enumerate_all_cliques(tmp_graph)
	for clique in cliques:
		if len(clique)==MaxCliqueLength:
			phase1_hypotheses.append([kCode]+clique)

MaxNCodes_AfterPhase1 = max([len(h) for h in phase1_hypotheses])
print MaxNCodes_AfterPhase1, "codes after phase 1 selection"

#phase 2: find all other nodes with hamming distance of at least 4 to the kCode and distance==4 w.r.t. to each other
print "Phase 2"
phase2_hypotheses = []
for phase1_hypothesis in tqdm(phase1_hypotheses, unit="Phase-1 hypotheses"):
	if len(phase1_hypothesis) != MaxNCodes_AfterPhase1:		#only continue with the most frequent
		continue

	kCode = phase1_hypothesis[0]		#kCode is at first position in the hypotheses
	
	phase2_candidates = []
	for candidateCode in codeCandidates[kCode]:
		if candidateCode in phase1_hypothesis:
			continue

		if hamming_distance(kCode, candidateCode)>=4:
			phase2_candidates.append(candidateCode)

	tmp_graph = nx.Graph()
	for node1, node2 in product(phase1_hypothesis+phase2_candidates, phase1_hypothesis+phase2_candidates):
		if int(node1, 2) < int(node2, 2):
			continue
		if hamming_distance(node1, node2)>=4:
			tmp_graph.add_edge(node1, node2)
	
	cliques = nx.clique.enumerate_all_cliques(tmp_graph)
	MaxCliqueLength = max([len(clique) for clique in cliques])
	if MaxCliqueLength+len(phase1_hypothesis) < 11:			#we want at least 11 codes after phase 2
		continue
	cliques = nx.clique.enumerate_all_cliques(tmp_graph)		#clique generator object disappears after being called above
	for clique in cliques:
		if len(clique)==MaxCliqueLength:
			phase2_hypotheses.append(phase1_hypothesis+np.setdiff1d(clique, phase1_hypothesis).tolist())

MaxNCodes_AfterPhase2 = max([len(h) for h in phase2_hypotheses])
print MaxNCodes_AfterPhase2, "after phase 2 selection"

print "Phase 3"
phase3_hypotheses = []
#phase 3: find all other nodes with hamming distance of at least 6 to the kCode 
for phase2_hypothesis in tqdm(phase2_hypotheses, unit="Phase-2 hypotheses"):
	if len(phase2_hypothesis) != MaxNCodes_AfterPhase2:		#only continue with the most frequent
		continue	

	kCode = phase2_hypothesis[0]
	
	phase3_candidates = []
	
	for candidateCode in codeCandidates[kCode]:
		if candidateCode in phase2_hypothesis:
			continue

		if hamming_distance(kCode, candidateCode)>=6:
			phase3_candidates.append(candidateCode)

	phase3_hypotheses.append(phase2_hypothesis+phase3_candidates)

MaxNCodes_AfterPhase3 = max([len(h) for h in phase3_hypotheses])
print MaxNCodes_AfterPhase3, "after phase 3 selection"


print "Phase 4"
phase4_hypotheses = []
#phase 3: find all other nodes with hamming distance of at least 4 to the kCode 
for phase3_hypothesis in tqdm(phase3_hypotheses, unit="Phase-3 hypotheses"):
	if len(phase3_hypothesis) != MaxNCodes_AfterPhase3:		#only continue with the most frequent
		continue	

	kCode = phase3_hypothesis[0]
	
	phase4_candidates = []
	
	for candidateCode in codeCandidates[kCode]:
		if candidateCode in phase3_hypothesis:
			continue

		if hamming_distance(kCode, candidateCode)>=4:
			phase4_candidates.append(candidateCode)

	phase4_hypotheses.append(phase3_hypothesis+phase4_candidates)

MaxNCodes_AfterPhase4 = max([len(h) for h in phase4_hypotheses])
print MaxNCodes_AfterPhase4, "after phase 4 selection"

final_codes = [h for h in phase4_hypotheses if len(h)==MaxNCodes_AfterPhase4]

headers = [code_priorities[prio] for prio in sorted(code_priorities)]
categories = np.array(len(headers)*["(Category 1)"])
categories[1:MaxNCodes_AfterPhase1] = "(Category 2)"
categories[MaxNCodes_AfterPhase1:MaxNCodes_AfterPhase2] = "(Category 3)"
categories[MaxNCodes_AfterPhase2:MaxNCodes_AfterPhase3] = "(Category 4)"
categories[MaxNCodes_AfterPhase3:MaxNCodes_AfterPhase4] = "(Category 5)"
categories[MaxNCodes_AfterPhase4:32] = "-"
table = [tuple(categories)]
for codes in final_codes:
	table.append(tuple(codes))

with open("codeBook_maximal.html", "w") as outfile:
	outfile.write(tabulate(table, headers=headers, tablefmt="html"))
