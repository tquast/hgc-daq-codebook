#Author: Thorben Quast, thorben.quast@cern.ch
#last modified: 20 March 2020
#Constructs the codebooks and the code - category assignments


from commands import code_priorities
from utility import *


from pprint import pprint
from collections import Counter
from itertools import product
import networkx as nx
import numpy as np
from tabulate import tabulate
from copy import deepcopy
from tqdm import tqdm


import argparse
parser = argparse.ArgumentParser()
parser.add_argument("--codeBase", type=str, help="select codes either from set of valid kCodes or all balanced dodes (kCodes/balancedCodes)", default="balancedCodes", required=False)
args = parser.parse_args()

#1. construct all possible 8-bit codes
allCodes = ['{:08b}'.format(value) for value in range(2**8)]

#2. disparity must be equal to zero (average voltage = 0.5V)
#count the disparity
pprint('Number of 8b codes by disparity:')
pprint(Counter([ disparity(code) for code in allCodes ]))

# Using only balanced codes makes sure that we can keep the line DC-balanced without special care
balancedCodes = [ code for code in allCodes if disparity(code)==0]
pprint('There are %i balanced codes.'%len(balancedCodes))

#3a. clock desynchronisation could occur --> kCode should only appear twice in pair
#3b. inversion: kCode should not appear at all
kCodeStatus = { kCode: isKCodeGood(kCode) for kCode in balancedCodes }
statusKCode = {} 
for kCode in balancedCodes:
    status = isKCodeGood(kCode)
    statusKCode[status] = statusKCode.get(status, [])
    statusKCode[status].append(kCode)
pprint('Status of kCodes regarding their appearing elsewhere in the idle (kCode) stream or in the inverted idle stream:')
pprint(statusKCode)
kCodeCandidates = statusKCode["OK"]


#code candidates for a specific kCode, i.e. those which do not have a collision
codeCandidates = {}		
print "Finding code candidates"
for kCode in kCodeCandidates:
	if args.codeBase == "kCodes":
		candidateCodes = kCodeCandidates	#choose candidates from pool of kCodes ...
	elif args.codeBase == "balancedCodes":
		candidateCodes = balancedCodes		# ... or from the balanced code
	else:
		import sys
		sys.exit("No valid codeBase!")

	codeCandidates[kCode] = removeCollisions(deepcopy(candidateCodes), deepcopy(kCode))
	print "kCode=",kCode,"has",len(codeCandidates[kCode]), "possible candidates"



#phase 1: find all codes with hamming distance of at least 6 to the kCode candidate
phase1_hypotheses = []
print "Category 1"
for kCode in tqdm(kCodeCandidates, unit="kCodes"):	
	phase1_candidates = []
	for candidateCode in codeCandidates[kCode]:
		if hamming_distance(kCode, candidateCode)>=6:
			phase1_candidates.append(candidateCode)

	#require that all Phase-1 code candidates have at hamming distance of at least 4 w.r.t. each other
	tmp_graph = nx.Graph()
	for node1, node2 in product(phase1_candidates, phase1_candidates):
		if int(node1, 2) < int(node2, 2):
			continue
		if hamming_distance(node1, node2)>=4:
			tmp_graph.add_edge(node1, node2)

	cliques = [clique for clique in nx.clique.enumerate_all_cliques(tmp_graph)]
	if len([clique for clique in cliques])==0:
		continue
	MaxCliqueLength = max([len(clique) for clique in cliques])
	for clique in cliques:
		if len(clique)==3:		#three high frequency codes
			phase1_hypotheses.append([kCode]+clique)

MaxNCodes_AfterPhase1 = max([len(h) for h in phase1_hypotheses])
print MaxNCodes_AfterPhase1, "codes after phase 1 selection"

#phase 2: find all other nodes with hamming distance of at least 4 to the kCode and distance==4 w.r.t. to each other
print "Category 2"
phase2_hypotheses = []
for phase1_hypothesis in tqdm(phase1_hypotheses, unit="Phase-1 hypotheses"):
	if len(phase1_hypothesis) != MaxNCodes_AfterPhase1:		#only continue with the most frequent
		continue

	kCode = phase1_hypothesis[0]		#kCode is at first position in the hypotheses
	
	phase2_code_candidates = []
	for candidateCode in codeCandidates[kCode]:
		if candidateCode in phase1_hypothesis:
			continue

		addCode = True
		for ph1_code in phase1_hypothesis:
			if hamming_distance(ph1_code, candidateCode)<4:
				addCode=False
				break
		if addCode:
			phase2_code_candidates.append(candidateCode)


	#find all phase2_candidates with HD = 2 to each other
	tmp_graph = nx.Graph()
	for node1, node2 in product(phase2_code_candidates, phase2_code_candidates):
		if int(node1, 2) < int(node2, 2):
			continue
		if hamming_distance(node1, node2)==2:
			tmp_graph.add_edge(node1, node2)

	cliques = [clique for clique in nx.clique.enumerate_all_cliques(tmp_graph) if len(clique)]
	maxNCliques = max([len(clique) for clique in cliques])
	unique_entries = []
	for clique in cliques:
		if len(clique) < maxNCliques:
			continue
		for clique_entry in clique:
			if not clique_entry in unique_entries:
				unique_entries.append(clique_entry)

	phase2_hypothesis = deepcopy(phase1_hypothesis)+unique_entries
	phase2_hypotheses.append(phase2_hypothesis)
	
MaxNCodes_AfterPhase2 = max([len(h) for h in phase2_hypotheses])
print MaxNCodes_AfterPhase2, "after category 2 selection"


print "Category 3"
phase3_hypotheses = []
for phase2_hypothesis in tqdm(phase2_hypotheses, unit="Phase-2 hypotheses"):
	if len(phase2_hypothesis) != MaxNCodes_AfterPhase2:		#only continue with the most frequent
		continue	

	kCode = phase2_hypothesis[0]
	
	phase3_candidates = []
	
	for candidateCode in codeCandidates[kCode]:
		if candidateCode in phase2_hypothesis:
			continue
		
		addCode = True
		for ph2_code in phase2_hypothesis[0:3]:
			if hamming_distance(ph2_code, candidateCode)<4:
				addCode=False
				break
		if addCode:
			phase3_candidates.append(candidateCode)

	phase3_hypotheses.append(phase2_hypothesis+phase3_candidates)

MaxNCodes_AfterPhase3 = max([len(h) for h in phase3_hypotheses])
print MaxNCodes_AfterPhase3, "after category 3 selection"


print "Category 4"
phase4_hypotheses = []
for phase3_hypothesis in tqdm(phase3_hypotheses, unit="Phase-3 hypotheses"):
	if len(phase3_hypothesis) != MaxNCodes_AfterPhase3:		#only continue with the most frequent
		continue	

	kCode = phase3_hypothesis[0]
	
	phase4_candidates = []
	
	for candidateCode in codeCandidates[kCode]:
		if candidateCode in phase3_hypothesis:
			continue

		addCodeCounter = 0
		for ph3_code in phase3_hypothesis[1:3]:
			if hamming_distance(ph3_code, candidateCode)>=4:
				addCodeCounter+=1
		if (hamming_distance(phase3_hypothesis[0], candidateCode)>=4) and (addCodeCounter>={"balancedCodes": 1, "kCodes": 0}[args.codeBase]):
			phase4_candidates.append(candidateCode)

	phase4_hypotheses.append(phase3_hypothesis+phase4_candidates)

MaxNCodes_AfterPhase4 = max([len(h) for h in phase4_hypotheses])
print MaxNCodes_AfterPhase4, "after category 4 selection"


min_Ncodes = min(32, MaxNCodes_AfterPhase4)

#only keep permutations with highest number of possible codes
final_codes = [[index]+h for index, h in enumerate(phase4_hypotheses) if len(h)>=min_Ncodes]		
#convert codes to decimal numbers
for code_index in range(len(final_codes)):
	for word_index in range(len(final_codes[code_index])):
		if type(final_codes[code_index][word_index])==str:
			final_codes[code_index][word_index] = int(final_codes[code_index][word_index], 2)


#dump codes to txt file with decimal numbers instead of binaries
headers = [code_priorities[prio] for prio in sorted(code_priorities)]
categories = np.array(len(headers)*["kCode"])
categories[1:MaxNCodes_AfterPhase1] = "(1)"
categories[MaxNCodes_AfterPhase1:MaxNCodes_AfterPhase2] = "(2)"
categories[MaxNCodes_AfterPhase2:MaxNCodes_AfterPhase3] = "(3)"
categories[MaxNCodes_AfterPhase3:MaxNCodes_AfterPhase4] = "(4)"
categories[MaxNCodes_AfterPhase4:32] = "-"
headers = ["Index"]+headers
categories = np.insert(categories, 0, "Index")
table = [tuple(categories)]
for codes in final_codes:
	table.append(tuple(codes))

with open("results/codeBook_from_%s.txt"%args.codeBase, "w") as outfile:
	outfile.write(tabulate(table, headers=headers))
